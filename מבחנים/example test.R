rm(list=ls())

#------Q1------

#read the data
documents <- read.csv("document categorization.csv", stringsAsFactors = FALSE)

#read the file, not defined string as factor
str(documents)

#Make category a factor (target value) 
documents$category <- as.factor(documents$category)
str(documents)

#tm is a text mining package with a bunch of text mining functions 
install.packages('tm')
library(tm)

#build a corpus from the text part of the file
documents.corpus <- Corpus(VectorSource(documents$text)) 

#build a dtm  from the corpus
dtm <- DocumentTermMatrix(documents.corpus)

#check dimensions of dtm
dim(dtm)

# cleainin the corpus

# remove puctuation
clean.corpus <- tm_map(documents.corpus,removePunctuation)
#remove digits
clean.corpus <- tm_map(clean.corpus,removeNumbers)
#turn to lower case
clean.corpus <- tm_map(clean.corpus,content_transformer(tolower))
clean.corpus[[1]][[1]]
#remove stopwords 
clean.corpus <- tm_map(clean.corpus,removeWords, stopwords())
#  Multiple whitespace characters are collapsed to a single blank
clean.corpus <- tm_map(clean.corpus,stripWhitespace)

#build a dtm  from the clean.corpus
dtm <- DocumentTermMatrix(clean.corpus)

#check dimensions of dtm
dim(dtm)

#removing infrequent terms 
frequent.dtm <-DocumentTermMatrix(clean.corpus,list(dictionary=findFreqTerms(dtm,4)))

#inspet the frequent_dtm
dim(frequent.dtm)
inspect(frequent.dtm[1:6,])

#------Q2------

#convert the frequency matrix to yes/no  
conv_yesno <- function(x){
  x <- ifelse(x>0,1,0)
  x <- factor(x, level = c(1,0), labels = c('Yes', 'No'))
}

#apply the function
dtm.yn <- apply(frequent.dtm, MARGIN = 1:2, conv_yesno)
dim(dtm.yn)

#convert the matrix into data frames 
dtm.yn <- as.data.frame(dtm.yn)
dim(dtm.yn)

#add category (economy or Sport) column 
dtm.yn$category <- documents$category
dim(dtm.yn)

#run naive base 
install.packages('e1071')
library(e1071)

dim(dtm.yn)

model.naive <- naiveBayes(dtm.yn[,-18],dtm.yn$category)
model.naive

#------Q3------

prediction.naive <- predict(model.naive, dtm.yn[,-18])
prediction.naive

confusion.naive <- table(prediction.naive, dtm.yn$category)
confusion.naive

TP.naive <- confusion.naive[2,2]
FP.naive <- confusion.naive[2,1]
TN.naive <- confusion.naive[1,1]
FN.naive <- confusion.naive[1,2]

recall.naive <- TP.naive/(TP.naive+FN.naive)
precision.naive <- TP.naive/(TP.naive+FP.naive)

recall.naive
precision.naive

#------Q4------
str(frequent.dtm)

#convert to matrix 
logistic.matrix <- as.matrix(frequent.dtm)

#convert to data frame 
logistic.df <- as.data.frame(logistic.matrix)
dim(logistic.df)

#add a category column
logistic.df$category <- documents$category
dim(logistic.df)

str(logistic.df)

#generate the logistic regression model 
model.logistic <- glm(category ~ ., family = binomial(link = 'logit'), data = logistic.df)
summary(model.logistic)

#------Q5------

predict.probabilities <- predict(model.logistic, logistic.df, type = 'response') 
predict.probabilities
predict.values <- ifelse(predict.probabilities > 0.5 ,1,0) 
predict.values
misClassError <- mean(predict.values != logistic.df$category)
misClassError

#confusion matrix 
confusion.logistic <- table(predict.values, logistic.df$category)
confusion.logistic

#------Q6------
set.seed(101)

#Run the model
dim(logistic.df) #18
Cluster <- kmeans(logistic.df[, 1:17], 2, nstart = 20)
Cluster$cluster #see the groups
table(Cluster$cluster, logistic.df$category)


